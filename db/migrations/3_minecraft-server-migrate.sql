-- Create the new table
CREATE TABLE IF NOT EXISTS minecraft
(
    server_discord   integer not null,
    server_minecraft text    not null,
    PRIMARY KEY (server_discord, server_minecraft),
    FOREIGN KEY (server_discord) REFERENCES servers (server)
);

-- Import in the data
INSERT INTO minecraft (server_discord, server_minecraft)
SELECT server, server_minecraft
FROM servers
where server_minecraft is not null;

-- delete the col in teh server table
ALTER TABLE servers
    DROP COLUMN server_minecraft; 