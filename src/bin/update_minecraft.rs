use skynet_discord_bot::{db_init, get_config, get_minecraft_config, update_server, whitelist_wipe};
use std::collections::HashSet;

#[tokio::main]
async fn main() {
  let config = get_config();
  let db = match db_init(&config).await {
    Ok(x) => x,
    Err(_) => return,
  };

  let servers = get_minecraft_config(&db).await;
  let mut wiped = HashSet::new();

  for server in &servers {
    // wipe whitelist first
    if !wiped.contains(&server.minecraft) {
      whitelist_wipe(&server.minecraft, &config.discord_minecraft).await;
      // add it to teh done list so its not done again
      wiped.insert(&server.minecraft);
    }

    update_server(&server.minecraft, &db, &server.discord, &config).await;
  }
}
