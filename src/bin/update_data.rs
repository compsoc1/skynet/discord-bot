use serenity::{
  async_trait,
  client::{Context, EventHandler},
  model::gateway::{GatewayIntents, Ready},
  Client,
};
use skynet_discord_bot::{db_init, get_config, get_data::get_wolves, Config, DataBase};
use std::{process, sync::Arc};
use tokio::sync::RwLock;

#[tokio::main]
async fn main() {
  let config = get_config();
  let db = match db_init(&config).await {
    Ok(x) => x,
    Err(_) => return,
  };

  // Intents are a bitflag, bitwise operations can be used to dictate which intents to use
  let intents = GatewayIntents::GUILDS | GatewayIntents::GUILD_MESSAGES | GatewayIntents::MESSAGE_CONTENT | GatewayIntents::GUILD_MEMBERS;
  // Build our client.
  let mut client = Client::builder(&config.discord_token, intents)
    .event_handler(Handler {})
    .await
    .expect("Error creating client");

  {
    let mut data = client.data.write().await;

    data.insert::<Config>(Arc::new(RwLock::new(config)));
    data.insert::<DataBase>(Arc::new(RwLock::new(db)));
  }

  if let Err(why) = client.start().await {
    println!("Client error: {:?}", why);
  }
}

struct Handler;
#[async_trait]
impl EventHandler for Handler {
  async fn ready(&self, ctx: Context, ready: Ready) {
    let ctx = Arc::new(ctx);
    println!("{} is connected!", ready.user.name);

    get_wolves(&ctx).await;

    // finish up
    process::exit(0);
  }
}
