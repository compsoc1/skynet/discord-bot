pub mod commands;

use serenity::{
  async_trait,
  client::{Context, EventHandler},
  model::{
    application::{command::Command, interaction::Interaction},
    gateway::{GatewayIntents, Ready},
    guild,
  },
  Client,
};
use std::sync::Arc;

use skynet_discord_bot::{db_init, get_config, get_server_config, get_server_member, Config, DataBase};
use tokio::sync::RwLock;

struct Handler;

#[async_trait]
impl EventHandler for Handler {
  async fn guild_member_addition(&self, ctx: Context, mut new_member: guild::Member) {
    let db_lock = {
      let data_read = ctx.data.read().await;
      data_read.get::<DataBase>().expect("Expected Config in TypeMap.").clone()
    };

    let db = db_lock.read().await;
    let config = match get_server_config(&db, &new_member.guild_id).await {
      None => return,
      Some(x) => x,
    };

    if get_server_member(&db, &new_member.guild_id, &new_member).await.is_ok() {
      let mut roles = vec![];

      if let Some(role) = &config.role_past {
        if !new_member.roles.contains(role) {
          roles.push(role.to_owned());
        }
      }

      if let Some(role) = &config.role_current {
        if !new_member.roles.contains(role) {
          roles.push(role.to_owned());
        }
      }

      if let Err(e) = new_member.add_roles(&ctx, &roles).await {
        println!("{:?}", e);
      }
    }
  }

  async fn ready(&self, ctx: Context, ready: Ready) {
    println!("[Main] {} is connected!", ready.user.name);

    match Command::set_global_application_commands(&ctx.http, |commands| {
      commands
        .create_application_command(|command| commands::add_server::register(command))
        .create_application_command(|command| commands::link_email::link::register(command))
        .create_application_command(|command| commands::link_email::verify::register(command))
        .create_application_command(|command| commands::minecraft::server::add::register(command))
        .create_application_command(|command| commands::minecraft::server::list::register(command))
        .create_application_command(|command| commands::minecraft::server::delete::register(command))
        .create_application_command(|command| commands::minecraft::user::add::register(command))
    })
    .await
    {
      Ok(_) => {}
      Err(e) => {
        println!("{:?}", e)
      }
    }
  }

  async fn interaction_create(&self, ctx: Context, interaction: Interaction) {
    if let Interaction::ApplicationCommand(command) = interaction {
      let _ = command.defer_ephemeral(&ctx.http).await;
      //println!("Received command interaction: {:#?}", command);

      let content = match command.data.name.as_str() {
        // user commands
        "link_wolves" => commands::link_email::link::run(&command, &ctx).await,
        "verify" => commands::link_email::verify::run(&command, &ctx).await,
        "link_minecraft" => commands::minecraft::user::add::run(&command, &ctx).await,
        // admin commands
        "add" => commands::add_server::run(&command, &ctx).await,
        "minecraft_add" => commands::minecraft::server::add::run(&command, &ctx).await,
        "minecraft_list" => commands::minecraft::server::list::run(&command, &ctx).await,
        "minecraft_delete" => commands::minecraft::server::delete::run(&command, &ctx).await,
        _ => "not implemented :(".to_string(),
      };

      if let Err(why) = command.edit_original_interaction_response(&ctx.http, |response| response.content(content)).await {
        println!("Cannot respond to slash command: {}", why);
      }
    }
  }
}

#[tokio::main]
async fn main() {
  let config = get_config();
  let db = match db_init(&config).await {
    Ok(x) => x,
    Err(_) => return,
  };

  // Intents are a bitflag, bitwise operations can be used to dictate which intents to use
  let intents = GatewayIntents::GUILDS | GatewayIntents::GUILD_MESSAGES | GatewayIntents::MESSAGE_CONTENT | GatewayIntents::GUILD_MEMBERS;
  // Build our client.
  let mut client = Client::builder(&config.discord_token, intents)
    .event_handler(Handler {})
    .await
    .expect("Error creating client");

  {
    let mut data = client.data.write().await;

    data.insert::<Config>(Arc::new(RwLock::new(config)));
    data.insert::<DataBase>(Arc::new(RwLock::new(db)));
  }

  // Finally, start a single shard, and start listening to events.
  //
  // Shards will automatically attempt to reconnect, and will perform
  // exponential backoff until it reconnects.
  if let Err(why) = client.start().await {
    println!("Client error: {:?}", why);
  }
}
