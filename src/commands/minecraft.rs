use serenity::{
  builder::CreateApplicationCommand,
  client::Context,
  model::{
    application::interaction::application_command::ApplicationCommandInteraction,
    prelude::{command::CommandOptionType, interaction::application_command::CommandDataOptionValue},
  },
};

use skynet_discord_bot::DataBase;
use sqlx::{Pool, Sqlite};

pub(crate) mod user {
  use super::*;
  pub(crate) mod add {
    use super::*;
    use crate::commands::link_email::link::get_server_member_discord;
    use serenity::model::id::UserId;
    use skynet_discord_bot::{whitelist_update, Config, Minecraft, Wolves};
    use sqlx::Error;

    pub fn register(command: &mut CreateApplicationCommand) -> &mut CreateApplicationCommand {
      command.name("link_minecraft").description("Link your minecraft account").create_option(|option| {
        option
          .name("minecraft-username")
          .description("Your Minecraft username")
          .kind(CommandOptionType::String)
          .required(true)
      })
    }

    pub async fn run(command: &ApplicationCommandInteraction, ctx: &Context) -> String {
      let db_lock = {
        let data_read = ctx.data.read().await;
        data_read.get::<DataBase>().expect("Expected Databse in TypeMap.").clone()
      };
      let db = db_lock.read().await;

      let config_lock = {
        let data_read = ctx.data.read().await;
        data_read.get::<Config>().expect("Expected Config in TypeMap.").clone()
      };
      let config = config_lock.read().await;

      // user has to have previously linked with wolves
      if get_server_member_discord(&db, &command.user.id).await.is_none() {
        return "Not linked with wolves, please use ``/link_wolves`` with your wolves email.".to_string();
      }

      let username = if let CommandDataOptionValue::String(username) = command
        .data
        .options
        .first()
        .expect("Expected username option")
        .resolved
        .as_ref()
        .expect("Expected username object")
      {
        username.trim()
      } else {
        return "Please provide a valid username".to_string();
      };

      // insert the username into the database
      match add_minecraft(&db, &command.user.id, username).await {
        Ok(_) => {}
        Err(e) => {
          dbg!("{:?}", e);
          return format!("Failure to minecraft username {:?}", username);
        }
      }

      // get a list of servers that the user is a member of
      if let Ok(servers) = get_servers(&db, &command.user.id).await {
        for server in servers {
          whitelist_update(&vec![username.to_string()], &server.minecraft, &config.discord_minecraft).await;
        }
      }

      "Added/Updated minecraft_user info".to_string()
    }

    async fn add_minecraft(db: &Pool<Sqlite>, user: &UserId, minecraft: &str) -> Result<Option<Wolves>, Error> {
      sqlx::query_as::<_, Wolves>(
        "
      UPDATE wolves
      SET minecraft = ?2
      WHERE discord = ?1; 
    ",
      )
      .bind(*user.as_u64() as i64)
      .bind(minecraft)
      .fetch_optional(db)
      .await
    }

    async fn get_servers(db: &Pool<Sqlite>, discord: &UserId) -> Result<Vec<Minecraft>, Error> {
      sqlx::query_as::<_, Minecraft>(
        "
      SELECT minecraft.*
      FROM minecraft
      JOIN (
              SELECT server
              FROM server_members
              JOIN wolves USING (id_wolves)
              WHERE discord = ?1
      ) sub on minecraft.server_discord = sub.server
    ",
      )
      .bind(*discord.as_u64() as i64)
      .fetch_all(db)
      .await
    }
  }
}

pub(crate) mod server {
  use super::*;

  pub(crate) mod add {
    use serenity::model::id::GuildId;
    use sqlx::Error;
    // this is to managfe the server side of commands related to minecraft
    use super::*;
    use skynet_discord_bot::{is_admin, update_server, Config, Minecraft};

    pub fn register(command: &mut CreateApplicationCommand) -> &mut CreateApplicationCommand {
      command.name("minecraft_add").description("Add a minecraft server").create_option(|option| {
        option
          .name("server_id")
          .description("ID of the Minecraft server hosted by the Computer Society")
          .kind(CommandOptionType::String)
          .required(true)
      })
    }

    pub async fn run(command: &ApplicationCommandInteraction, ctx: &Context) -> String {
      // check if user has high enough permisssions
      if let Some(msg) = is_admin(command, ctx).await {
        return msg;
      }
      let g_id = match command.guild_id {
        None => return "Not in a server".to_string(),
        Some(x) => x,
      };

      let server_minecraft = if let CommandDataOptionValue::String(id) = command
        .data
        .options
        .first()
        .expect("Expected server_id option")
        .resolved
        .as_ref()
        .expect("Expected server_id object")
      {
        id.to_owned()
      } else {
        return String::from("Expected Server ID");
      };

      let db_lock = {
        let data_read = ctx.data.read().await;
        data_read.get::<DataBase>().expect("Expected Databse in TypeMap.").clone()
      };
      let db = db_lock.read().await;

      match add_server(&db, &g_id, &server_minecraft).await {
        Ok(_) => {}
        Err(e) => {
          println!("{:?}", e);
          return format!("Failure to insert into Minecraft {} {}", &g_id, &server_minecraft);
        }
      }

      let config_lock = {
        let data_read = ctx.data.read().await;
        data_read.get::<Config>().expect("Expected Config in TypeMap.").clone()
      };
      let config = config_lock.read().await;

      update_server(&server_minecraft, &db, &g_id, &config).await;

      "Added/Updated minecraft_server info".to_string()
    }

    async fn add_server(db: &Pool<Sqlite>, discord: &GuildId, minecraft: &str) -> Result<Option<Minecraft>, Error> {
      sqlx::query_as::<_, Minecraft>(
        "
      INSERT OR REPLACE INTO minecraft (server_discord, server_minecraft)
      VALUES (?1, ?2)
    ",
      )
      .bind(*discord.as_u64() as i64)
      .bind(minecraft)
      .fetch_optional(db)
      .await
    }
  }

  pub(crate) mod list {
    use serenity::builder::CreateApplicationCommand;
    use serenity::client::Context;
    use serenity::model::prelude::application_command::ApplicationCommandInteraction;
    use skynet_discord_bot::{get_minecraft_config_server, is_admin, server_information, Config, DataBase};

    pub fn register(command: &mut CreateApplicationCommand) -> &mut CreateApplicationCommand {
      command.name("minecraft_list").description("List your minecraft servers")
    }

    pub async fn run(command: &ApplicationCommandInteraction, ctx: &Context) -> String {
      if let Some(msg) = is_admin(command, ctx).await {
        return msg;
      }
      let g_id = match command.guild_id {
        None => return "Not in a server".to_string(),
        Some(x) => x,
      };

      let db_lock = {
        let data_read = ctx.data.read().await;
        data_read.get::<DataBase>().expect("Expected Databse in TypeMap.").clone()
      };
      let db = db_lock.read().await;

      let servers = get_minecraft_config_server(&db, g_id).await;

      if servers.is_empty() {
        return "No minecraft servers, use /minecraft_add to add one".to_string();
      }

      let config_lock = {
        let data_read = ctx.data.read().await;
        data_read.get::<Config>().expect("Expected Config in TypeMap.").clone()
      };
      let config = config_lock.read().await;

      let mut result = "Server Information:\n".to_string();
      for server in get_minecraft_config_server(&db, g_id).await {
        if let Some(x) = server_information(&server.minecraft, &config.discord_minecraft).await {
          result.push_str(&format!(
            r#"
            Name:   {name}
            ID:     {id}
            Online: {online}
            Info:   {description} 
            Link:   <http://panel.games.skynet.ie/server/{id}>
            "#,
            name = &x.attributes.name,
            online = !x.attributes.is_suspended,
            description = &x.attributes.description,
            id = &x.attributes.identifier
          ));
        }
      }
      result.to_string()
    }
  }

  pub(crate) mod delete {
    use serenity::builder::CreateApplicationCommand;
    use serenity::client::Context;
    use serenity::model::application::command::CommandOptionType;
    use serenity::model::id::GuildId;
    use serenity::model::prelude::application_command::{ApplicationCommandInteraction, CommandDataOptionValue};
    use skynet_discord_bot::{is_admin, DataBase, Minecraft};
    use sqlx::{Error, Pool, Sqlite};

    pub fn register(command: &mut CreateApplicationCommand) -> &mut CreateApplicationCommand {
      command.name("minecraft_delete").description("Delete a minecraft server").create_option(|option| {
        option
          .name("server_id")
          .description("ID of the Minecraft server hosted by the Computer Society")
          .kind(CommandOptionType::String)
          .required(true)
      })
    }

    pub async fn run(command: &ApplicationCommandInteraction, ctx: &Context) -> String {
      // check if user has high enough permisssions
      if let Some(msg) = is_admin(command, ctx).await {
        return msg;
      }
      let g_id = match command.guild_id {
        None => return "Not in a server".to_string(),
        Some(x) => x,
      };

      let server_minecraft = if let CommandDataOptionValue::String(id) = command
        .data
        .options
        .first()
        .expect("Expected server_id option")
        .resolved
        .as_ref()
        .expect("Expected server_id object")
      {
        id.to_owned()
      } else {
        return String::from("Expected Server ID");
      };

      let db_lock = {
        let data_read = ctx.data.read().await;
        data_read.get::<DataBase>().expect("Expected Databse in TypeMap.").clone()
      };
      let db = db_lock.read().await;

      match server_remove(&db, &g_id, &server_minecraft).await {
        Ok(_) => {}
        Err(e) => {
          println!("{:?}", e);
          return format!("Failure to insert into Minecraft {} {}", &g_id, &server_minecraft);
        }
      }

      // no need to clear teh whitelist as it will be reset within 24hr anyways

      "Removed minecraft_server info".to_string()
    }

    async fn server_remove(db: &Pool<Sqlite>, discord: &GuildId, minecraft: &str) -> Result<Option<Minecraft>, Error> {
      sqlx::query_as::<_, Minecraft>(
        "
      DELETE FROM minecraft
      WHERE server_discord = ?1 AND server_minecraft = ?2
    ",
      )
      .bind(*discord.as_u64() as i64)
      .bind(minecraft)
      .fetch_optional(db)
      .await
    }
  }
}
