use serenity::{
  builder::CreateApplicationCommand,
  client::Context,
  model::{
    application::interaction::application_command::ApplicationCommandInteraction,
    prelude::{command::CommandOptionType, interaction::application_command::CommandDataOptionValue},
  },
};
use skynet_discord_bot::get_data::get_wolves;
use skynet_discord_bot::{get_server_config, is_admin, set_roles::update_server, DataBase, Servers};
use sqlx::{Error, Pool, Sqlite};

pub async fn run(command: &ApplicationCommandInteraction, ctx: &Context) -> String {
  // check if user has high enough permisssions
  if let Some(msg) = is_admin(command, ctx).await {
    return msg;
  }

  let api_key = if let CommandDataOptionValue::String(key) = command
    .data
    .options
    .first()
    .expect("Expected user option")
    .resolved
    .as_ref()
    .expect("Expected user object")
  {
    key
  } else {
    return "Please provide a wolves API key".to_string();
  };

  let role_current = if let CommandDataOptionValue::Role(role) = command
    .data
    .options
    .get(1)
    .expect("Expected role option")
    .resolved
    .as_ref()
    .expect("Expected role object")
  {
    Some(role.id.to_owned())
  } else {
    return "Please provide a valid role for ``Role Current``".to_string();
  };

  let mut role_past = None;
  if let Some(x) = command.data.options.get(2) {
    if let Some(CommandDataOptionValue::Role(role)) = &x.resolved {
      role_past = Some(role.id.to_owned());
    }
  };

  let db_lock = {
    let data_read = ctx.data.read().await;
    data_read.get::<DataBase>().expect("Expected Databse in TypeMap.").clone()
  };
  let db = db_lock.read().await;

  let server_data = Servers {
    server: command.guild_id.unwrap_or_default(),
    wolves_api: api_key.to_owned(),
    role_past,
    role_current,
    member_past: 0,
    member_current: 0,
  };

  match add_server(&db, ctx, &server_data).await {
    Ok(_) => {}
    Err(e) => {
      println!("{:?}", e);
      return format!("Failure to insert into Servers {:?}", server_data);
    }
  }

  "Added/Updated server info".to_string()
}

pub fn register(command: &mut CreateApplicationCommand) -> &mut CreateApplicationCommand {
  command
    .name("add")
    .description("Enable the bot for this discord")
    .create_option(|option| {
      option
        .name("api_key")
        .description("UL Wolves API Key")
        .kind(CommandOptionType::String)
        .required(true)
    })
    .create_option(|option| {
      option
        .name("role_current")
        .description("Role for Current members")
        .kind(CommandOptionType::Role)
        .required(true)
    })
    .create_option(|option| {
      option
        .name("role_past")
        .description("Role for Past members")
        .kind(CommandOptionType::Role)
        .required(false)
    })
}

async fn add_server(db: &Pool<Sqlite>, ctx: &Context, server: &Servers) -> Result<Option<Servers>, Error> {
  let existing = get_server_config(db, &server.server).await;
  let role_past = server.role_past.map(|x| *x.as_u64() as i64);
  let role_current = server.role_current.map(|x| *x.as_u64() as i64);

  let insert = sqlx::query_as::<_, Servers>(
    "
      INSERT OR REPLACE INTO servers (server, wolves_api, role_past, role_current)
      VALUES (?1, ?2, ?3, ?4)
    ",
  )
  .bind(*server.server.as_u64() as i64)
  .bind(&server.wolves_api)
  .bind(role_past)
  .bind(role_current)
  .fetch_optional(db)
  .await;

  // if the entry does not exist already tehn do a user update
  let (update, current_remove, current_role, past_remove, past_role) = match &existing {
    None => (true, false, None, false, None),
    Some(x) => {
      let mut result = (false, false, None, false, None);
      if x.wolves_api != server.wolves_api {
        result.0 = true;
      }
      if x.role_current != server.role_current {
        result.0 = true;
        result.1 = true;
        result.2 = x.role_current;
      }
      if x.role_past != server.role_past {
        result.0 = true;
        result.3 = true;
        result.4 = x.role_past;
      }
      result
    }
  };

  // update all users
  if update {
    // handle wolves api here
    get_wolves(ctx).await;

    let mut roles_remove = vec![];
    if current_remove {
      roles_remove.push(current_role)
    }
    if past_remove {
      roles_remove.push(past_role)
    }
    update_server(ctx, server, &roles_remove, &[]).await;
  }

  insert
}
