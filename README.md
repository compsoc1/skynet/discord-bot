# Skynet Discord Bot
This bots core purpose is to give members roles based on their status on <ulwolves.ie>.  
It uses an api key provided by wolves to get member lists.  

Users are able to link their wolves account to the bot and that works across discord servers.  
For example is a user links on the CompSoc Discord then they will also get their roles (automagically) on Games Dev if they are a member there.

## Commands - Admin

You need admin access to run any of the commands in this section.  
Either the server owner or a suer with the ``Administrator`` permission 

### Getting the Skynet Discord bot
1. Email ``keith@assurememberships.com`` from committee email and say ye want an api key for ``193.1.99.74``
2. Create a role for current members (maybe call it ``current-member`` ?)
3. (Optional) create a role for all past and current members (ye can use the existing ``member`` role for this, )
4. Invite the bot https://discord.com/api/oauth2/authorize?client_id=1145761669256069270&permissions=139855185984&scope=bot
5. Make sure the bot role ``@skynet`` is above these two roles (so it can manage them)
6. Make sure that you have a role that gives ye administrator powers
7. Use the command ``/add`` and insert the api key, role current and role all (desktop recommended)

The reason for both roles is ye have one for active members while the second is for all current and past members.

### Minecraft
The bot is able to manage the whitelist of a Minecraft server managed by the Computer Society.  
Talk to us to get a server.

#### Add
This links a minecraft server with your club/society.

``/minecraft_add SERVER_ID``


#### List
List the servers linked to your club/society.  
It is possible to have more than one minecraft server

``/minecraft_list``

#### Delete
This unlinks a minecraft server from your club/society.

``/minecraft_delete SERVER_ID``

## Commands - User

### Setup
* Start the process using ``/link_wolves WOLVES_EMAIL`` 
  * The email that is in the Contact Email here: <https://ulwolves.ie/memberships/profile>
* An email will be sent to them that they need to verify using ``/verify CODE``
* This will only have to be done once.

* If the user is an active member on wolves
  * If they are in any servers with teh Skynet Bot
    * They will get relevant roles.
  * If they Join a server with teh bot enabled.
    * They will be granted the roles automatically
* If the user is **not** an active member on wolves.
  * If they have no Roles
    * No change
  * If they have Past Member Role
    * No change
  * If they have both Roles
    * The current-member role will be removed from them
    * Past Member role will remain unchanged

### Minecraft
Users can link their Minecraft username to grant them access to any servers where teh whitelist is managed by teh bot.

``/link_minecraft MINECRAFT_USERNAME``
